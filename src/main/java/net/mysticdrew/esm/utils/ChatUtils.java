package net.mysticdrew.esm.utils;

import static net.minecraft.util.EnumChatFormatting.*;

import java.util.HashMap;
import java.util.Map;

import com.mojang.authlib.GameProfile;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.Command;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.user.PlayerUtils;

public class ChatUtils {
	public static ChatUtils instance = new ChatUtils();
	
	public void sendChat(ICommandSender sender, String text) {
		sender.addChatMessage(new ChatComponentTranslation(text));
	}
	
	public String chatEvent(String username) {
		return formatedName(username);
	}
	
	public String formatedName(String username) {
		GameProfile userProf = PlayerUtils.instance.getUserByName(username);
		User user = FileUtils.instance.loadUser(userProf.getId());
		EnumChatFormatting color = WHITE;
		if (user != null) {
			if (user.getNameColor() != null) {
				color = user.getNameColor() ;
			}
			username = color + username + RESET;
			return  "[" + getTag(userProf.getName()) + username + "]";
		}
		return username;
	}
	
	private String getTag(String player) {
		if (PlayerUtils.instance.isOp(player)) {
			String optxt = ITALIC + "(Admin) " + RESET;
			optxt = RED + optxt + RESET;
			return optxt;
		}
		return "";
	}
	
	private String filteredMessage(String message) {

		return message;
	}
	
	public Map<String, EnumChatFormatting> getColorMap() {
		Map<String, EnumChatFormatting> colorMap = new HashMap<String, EnumChatFormatting>();
		colorMap.put("black", BLACK);
		colorMap.put("dark_blue", DARK_BLUE);
		colorMap.put("dark_green", DARK_GREEN);
		colorMap.put("dark_aqua", DARK_AQUA);
		colorMap.put("dark_purple", DARK_PURPLE);
		colorMap.put("dark_red", DARK_RED);
		colorMap.put("dark_gray", DARK_GRAY);
		colorMap.put("gold", GOLD);
		colorMap.put("gray", GRAY);
		colorMap.put("blue", BLUE);
		colorMap.put("green", GREEN);
		colorMap.put("aqua", AQUA);
		colorMap.put("red", RED);
		colorMap.put("light_purple", LIGHT_PURPLE);
		colorMap.put("yellow", YELLOW);
		colorMap.put("white", WHITE);
		return colorMap;
	}
	
	public String[] getColorList() {
		String[] colors = new String[getColorMap().size()];
		int i = 0;
		for (Map.Entry<String, EnumChatFormatting> map : getColorMap().entrySet()) {
			colors[i] = map.getKey();
			i++;
		}
		return colors;
	}
	
	public String getColorListWithColor() {
		StringBuilder colors = new StringBuilder();
		for (Map.Entry<String, EnumChatFormatting> map : getColorMap().entrySet()) {
			colors.append(map.getValue() + map.getKey() + RESET + "; ");

		}
		colors.replace(colors.length() - 2, colors.length(), "");
		return colors.toString();
	}
}
