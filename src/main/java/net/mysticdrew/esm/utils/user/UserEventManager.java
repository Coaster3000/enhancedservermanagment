package net.mysticdrew.esm.utils.user;

import java.util.Date;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

import net.minecraft.entity.player.EntityPlayer;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.ServerUtils;

public class UserEventManager {
	public static UserEventManager instance = new UserEventManager();
	private PlayerUtils playerUtil = PlayerUtils.instance;
	
	public void playerLogin(EntityPlayer player) {
		UUID uuid = player.getUniqueID();				
		User user = FileUtils.instance.loadUser(uuid);
		if (user == null) {
			user = createNewUser(playerUtil.getPlayerInfoById(uuid));
		}
		ServerUtils.instance.addUserToWorld(user.getName());
		loadExistingUser(user);
	}
	
	public void playerLogout(EntityPlayer player) {
		
	}
	
	public User createNewUser(GameProfile userProfile) {
		User user = new User();
		ServerUtils.instance.addUserToWorld(userProfile.getName());
		user.setName(userProfile.getName());
		user.setId(userProfile.getId());
		return user;
	}
	
	public void playerDied(EntityPlayer player) {
		User user = EnhancedServerModeration.userMap.get(player.getUniqueID());
		if (user.getDeaths() == null) {
			user.setDeaths(0L);
		} 
		user.setDeaths(user.getDeaths() + 1L);
		FileUtils.instance.saveUser(user);
	}
	
	private void loadExistingUser(User user) {
		user.setPlayerIP(playerUtil.getPlayerIp(user.getName()));
		user.setLoginDate(new Date());
		FileUtils.instance.saveUser(user);
		addUserToMap(user);
	}
	
	private void addUserToMap(User user) {
		EnhancedServerModeration.userMap.put(user.getId(), user);
	}
}
