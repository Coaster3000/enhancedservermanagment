/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.utils;

import org.apache.logging.log4j.Level;

import net.mysticdrew.esm.reference.Reference;
import cpw.mods.fml.common.FMLLog;

public class LogHelper {
	public static void log(Level level, Object o) {
		FMLLog.log(Reference.MOD_NAME, level, String.valueOf(o));
	}
	
	public static void all(Object o) {
		log(Level.ALL, o);
	}

	public static void debug(Object o) {
		log(Level.DEBUG, o);
	}

	public static void error(Object o) {
		log(Level.ERROR, o);
	}

	public static void fatal(Object o) {
		log(Level.FATAL, o);
	}

	public static void info(Object o) {
		log(Level.INFO, o);
	}

	public static void off(Object o) {
		log(Level.OFF, o);
	}

	public static void trace(Object o) {
		log(Level.TRACE, o);
	}

	public static void warn(Object o) {
		log(Level.WARN, o);
	}
}
